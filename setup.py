from setuptools import setup

setup(
    name = 'stool',
    version = '0.0.1',
    packages = ['cli', 'cli.cmd', 'stool'],
    include_package_data = True,
    install_requires = [
        'click',
        'steam',
        'tabulate'
    ],
    entry_points = """
        [console_scripts]
        stool=cli.cli:cli
        """,
)
