import os
import re

from requests.exceptions import HTTPError
from steam.webapi import WebAPI

from .config import Config

class Api(WebAPI):
    def __init__(self, key=Config.STEAMAPI_KEY):
        if key is not None:
            try:
                super().__init__(key=key)
            except HTTPError as e:
                if e.response.status_code == 403:
                    raise InvalidSteamApiKeyError()
        else:
            raise MissingSteamApiKeyError()

    def create_game_server_accounts(self, appid, memo='', count=1):
        call = self.IGameServersService.CreateAccount
        return [lambda: call(appid=appid, memo=memo)['response'] for i in range(count)]

    def get_game_server_accounts(self, steamid=None, memo_regex=None,
                                 appid=None, only_expired=False,
                                 only_deleted=False, only_valid=False):

        try:
            resp = self.IGameServersService.GetAccountList()['response']['servers']
        except KeyError:
            return []

        if steamid is not None:
            return [x for x in resp if x.get('steamid') == steamid]

        if only_expired and not only_valid:
            resp = [x for x in resp if x.get('is_expired')]

        if only_deleted and not only_valid:
            resp = [x for x in resp if x.get('is_deleted')]

        if only_valid:
            resp = [x for x in resp if not x.get('is_deleted') and not x.get('is_expired')]

        if appid is not None:
            resp = [x for x in resp if x.get('appid') == appid]

        if memo_regex is not None:
            p = re.compile(memo_regex)
            resp = [x for x in resp if p.search(x.get('memo', ''))]

        return resp

    def delete_game_server_accounts(self, steamid=None, memo_regex=None,
                                    appid=None, only_expired=False,
                                    only_valid=False):

        accs = self.get_game_server_accounts(memo_regex=memo_regex,
                                             steamid=steamid, appid=appid,
                                             only_expired=only_expired,
                                             only_valid=only_valid)
        call = self.IGameServersService.DeleteAccount
        return [lambda x=x: call(steamid=x.get('steamid')) for x in accs]

    def reset_login_tokens(self, steamid=None, appid=None, only_expired=False):
        accs = self.get_game_server_accounts(steamid=steamid, appid=appid,
                                             only_expired=only_expired)
        call = self.IGameServersService.ResetLoginToken
        return [lambda x=x: call(steamid=x.get('steamid')) for x in accs]

class InvalidSteamApiKeyError(Exception):
    def __init__(self, message='Invalid Steam API key!'):
        Exception.__init__(self, message)

        self.message = message
        self.exit_code = 1

    def __str__(self):
        return 'Error: ' + self.message

class MissingSteamApiKeyError(Exception):
    def __init__(self, message='No Steam API key set.'):
        Exception.__init__(self, message)

        self.message = message
        self.exit_code = 1

    def __str__(self):
        return 'Error: ' + self.message
