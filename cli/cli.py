import os

import click

CMD_DIR = os.path.join(os.path.dirname(__file__), 'cmd')

class CLI(click.MultiCommand):
    def list_commands(self, ctx):
        cmds = []

        for filename in os.listdir(CMD_DIR):
            if filename.endswith('.py'):
                cmds.append(filename[:-3])

        return cmds

    def get_command(self, ctx, name):
        ns = {}

        filename = os.path.join(CMD_DIR, name + '.py')

        try:
            with open(filename) as f:
                code = compile(f.read(), filename, 'exec')
                eval(code, ns, ns)

            return ns['cli']
        except FileNotFoundError:
            raise click.UsageError('No such command "{}".'.format(name), ctx=ctx)

@click.command(cls=CLI)
def cli():
    pass

if __name__ == '__main__':
    cli()
