import sys

import click
from steam.steamid import SteamID

@click.group()
def cli():
    """ SteamID operations """
    pass

@click.command('convert', help='Convert SteamID to various formats')
@click.option('-f', '--format', 'to_format',
              default='steam64',
              help='Specify the representation format (default: steam64)')
@click.argument('steamid')
def convert_steamid(steamid, to_format):
    sid = SteamID(steamid)

    if not sid.is_valid():
        click.echo('Error: Invalid Steam ID')
        sys.exit(1)

    if to_format == 'steam64':
        click.echo(sid.as_64)
    elif to_format == 'steam32':
        click.echo(sid.as_32)
    elif to_format == 'steam2':
        click.echo(sid.as_steam2)
    elif to_format == 'steam3':
        click.echo(sid.as_steam3)
    elif to_format == 'orangebox':
        click.echo(sid.as_steam2_zero)
    else:
        click.echo('Error: Unknown format')
        sys.exit(1)

cli.add_command(convert_steamid)
