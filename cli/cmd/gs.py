import sys

import click
from tabulate import tabulate

from stool.webapi import Api, MissingSteamApiKeyError, InvalidSteamApiKeyError


@click.group()
def cli():
    """ Steam Game Servers Service operations """
    pass

@click.command(help='Create game server accounts and get tokens.')
@click.option('-c', '--count', default=1, help='Number of tokens to generate.')
@click.option('-t', '--tokens-only', is_flag=True, help='Print just the login tokens.')
@click.argument('appid')
@click.argument('memo', required=False)
def create_account(appid, memo, count, tokens_only):
    api = _get_api()
    accs = api.create_game_server_accounts(appid, memo, count)
    with click.progressbar(accs, label=f'Creating {count} accounts') as bar:
        resp = [a() for a in bar]
    if tokens_only:
        click.echo('\n'.join((x['login_token'] for x in resp)))
    else:
        click.echo(tabulate(resp, headers='keys'))
cli.add_command(create_account)


@click.command(help='List game server accounts')
@click.option('-f', '--filter', 'filter_', required=False,
              help='Regex string to filter account memos.')
@click.option('-a', '--appid', required=False, type=click.INT,
              help='Return accounts for specific app id.')
@click.option('-d', '--deleted', 'only_deleted', required=False,
              is_flag=True, help='Return only deleted accounts.')
@click.option('-e', '--expired', 'only_expired', required=False,
              is_flag=True, help='Return only accounts with expired tokens.')
@click.option('-v', '--valid', 'only_valid', required=False,
              is_flag=True, help='Return only accounts with valid tokens.')
def list_accounts(filter_, appid, only_deleted, only_expired, only_valid):
    api = _get_api()
    click.echo(
        tabulate(
            api.get_game_server_accounts(memo_regex=filter_, appid=appid,
                                         only_deleted=only_deleted,
                                         only_expired=only_expired,
                                         only_valid=only_valid),
            headers='keys'
        ))
cli.add_command(list_accounts)


@click.command(help='Delete game server accounts')
@click.option('-f', '--filter', 'filter_', required=False,
              help='Regex string to filter account memos.')
@click.option('-a', '--appid', required=False, type=click.INT,
              help='Delete accounts for specific app id.')
@click.option('-e', '--expired', 'only_expired', required=False,
              is_flag=True, help='Delete only accounts with expired tokens.')
@click.option('-v', '--valid', 'only_valid', required=False,
              is_flag=True, help='Delete only accounts with valid tokens.')
@click.option('-y', '--yes', 'assume_yes', required=False, is_flag=True,
              help='Assume yes. Don\'t ask for confirmation.')
@click.argument('steamid', required=False)
def delete_accounts(filter_, steamid, appid, only_expired, only_valid, assume_yes):
    api = _get_api()
    accs = api.delete_game_server_accounts(memo_regex=filter_, steamid=steamid,
                                           appid=appid,
                                           only_expired=only_expired,
                                           only_valid=only_valid)
    if not accs: click.echo('No accounts to delete.'); return
    if not assume_yes: click.confirm(f'Deleting {len(accs)} accounts, proceed?',
                                     abort=True)
    with click.progressbar(accs, label=f'Deleting {len(accs)} accounts') as bar:
        for delacc in bar: delacc()

cli.add_command(delete_accounts)


@click.command(help='Renew game server login tokens.')
@click.option('-a', '--appid', required=False, type=click.INT,
              help='Renew login tokens for specific app id.')
@click.option('-e', '--expired', 'only_expired', required=False,
              is_flag=True, help='Delete only accounts with expired tokens.')
@click.argument('steamid', required=False)
def renew_tokens(steamid, appid, only_expired):
    api = _get_api()
    accs = api.reset_login_tokens(steamid=steamid, appid=appid,
                                           only_expired=only_expired)
    if not accs: click.echo('No accounts to renew tokens for.'); return
    with click.progressbar(accs, label=f'Renewing {len(accs)} tokens') as bar:
        for resettok in bar: resettok()

cli.add_command(renew_tokens)

def _get_api():
    api = None
    if api is None:
        try:
            api = Api()
        except (MissingSteamApiKeyError, InvalidSteamApiKeyError) as e:
            click.echo(e)
            sys.exit(e.exit_code)
    return api

